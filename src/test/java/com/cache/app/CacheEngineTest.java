package com.cache.app;

import com.cache.interfaces.ICacheEngine;
import jdk.nashorn.internal.objects.annotations.Property;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CacheEngineTest {

    @Property
    private ICacheEngine<Integer, Person> cacheEngine;

    @Property
    private Person person1, person2, person3;

    @Before
    public void setup() {
        person1 = new Person(1, "A");
        person2 = new Person(2, "B");
        person3 = new Person(3, "C");
        cacheEngine = new CacheEngine<Integer, Person>();
    }

    @Test
    public void containsItem() {
        assertFalse(cacheEngine.containsItem(1));
        cacheEngine.cacheItem(person2.getID(), person2);
        assertTrue(cacheEngine.containsItem(person2.getID()));
    }

    @Test
    public void getItem() {
        assertNull(cacheEngine.getItem(1));
        cacheEngine.cacheItem(person1.getID(), person1);
        assertNotNull(cacheEngine.getItem(person1.getID()));
    }

    @Test
    public void cacheItem() {
        assertTrue(cacheEngine.cacheItem(person1.getID(), person1));
        assertFalse(cacheEngine.cacheItem(person1.getID(), person1));
        assertTrue(cacheEngine.cacheItem(person2.getID(), person2));
        assertEquals(person1, cacheEngine.getItem(person1.getID()));
        assertEquals(person2, cacheEngine.getItem(person2.getID()));
    }

    @Test
    public void pruneItem() {
        cacheEngine.cacheItem(person1.getID(), person1);
        cacheEngine.cacheItem(person2.getID(), person2);
        assertTrue(cacheEngine.pruneItem());
        assertFalse(cacheEngine.containsItem(person1.getID()));
    }
}