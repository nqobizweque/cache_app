package com.cache.app;

import com.sun.tools.javac.util.Pair;
import jdk.nashorn.internal.objects.annotations.Property;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class ListItemTest {

    @Property
    private ListItem<Integer, Person> listItem1, listItem2, listItem3;

    @Property
    private Person person1, person2, person3;

    @Before
    public void setup() {
        person1 = new Person(1, "A");
        person2 = new Person(2, "B");
        person3 = new Person(3, "C");

        listItem1 = new ListItem<Integer, Person>(1, person1);
        listItem2 = new ListItem<Integer, Person>(2, person2);
        listItem3 = new ListItem<Integer, Person>(3, person3);
    }

    @Test
    public void testGetItem() {
        assertEquals(person1, listItem1.getItem());
    }

    @Test
    public void testSetHeadGetHead() {
        assertNull(listItem1.getHead());
        this.listItem1.setHead(listItem2);
        assertEquals(listItem1.getHead(), listItem2);
        listItem1.setTail(listItem3);
        assertEquals(listItem1.getTail(), listItem3);
    }

    @Test
    public void testSetTailGetTail() {
        assertNull(listItem1.getTail());
        listItem1.setTail(listItem2);
        assertEquals(listItem1.getTail(), listItem2);
    }

    @Test
    public void testSetKeyGetKey() {
        assertEquals(1, (int)listItem1.getKey());
        listItem1.setKey(4);
        assertEquals(4, (int)listItem1.getKey());
    }

    @Test
    public void testClear() {
        listItem2.setHead(listItem1);
        listItem2.setTail(listItem3);
        assertEquals(2, (int) listItem2.getKey());
        assertEquals(listItem1, listItem2.getHead());
        assertEquals(listItem3, listItem2.getTail());
    }
}