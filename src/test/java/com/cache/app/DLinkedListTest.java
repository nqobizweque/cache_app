package com.cache.app;

import com.cache.interfaces.IDLinkedList;
import com.cache.interfaces.IListItem;
import jdk.nashorn.internal.objects.annotations.Property;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class DLinkedListTest {

    @Property
    private IDLinkedList<Integer, Person> dLinkedList;

    @Property
    private IListItem<Integer, Person> listItem1, listItem2, listItem3;

    @Before
    public void setup() {
        listItem1 = new ListItem<Integer, Person>(1, new Person(1, "A"));
        listItem2 = new ListItem<Integer, Person>(2, new Person(2, "B"));
        listItem3 = new ListItem<Integer, Person>(3, new Person(3, "C"));
        dLinkedList = new DLinkedList<Integer, Person>();
    }

    @Test
    public void testAddItemGetHeadGetTailGetCountCheckLink() {
        assertNull(dLinkedList.getHead());
        assertNull(dLinkedList.getTail());
        assertEquals(0, dLinkedList.getCount());
        dLinkedList.addItem(listItem1);
        assertEquals(listItem1, dLinkedList.getHead());
        assertEquals(1, dLinkedList.getCount());
        dLinkedList.addItem(listItem2);
        assertEquals(listItem2, dLinkedList.getTail());
        assertEquals(listItem2, dLinkedList.getHead().getTail());
        assertEquals(listItem1, dLinkedList.getTail().getHead());
        dLinkedList.addItem(listItem3);
        assertEquals(listItem3, dLinkedList.getTail());
        assertEquals(listItem2, dLinkedList.getHead().getTail());
        assertEquals(listItem2, dLinkedList.getTail().getHead());
        assertEquals(3, dLinkedList.getCount());
        assertTrue(dLinkedList.removeListItem(listItem1));
        assertEquals(2, dLinkedList.getCount());
        assertEquals(dLinkedList.getHead(), listItem2);
        assertTrue(dLinkedList.removeListItem(listItem2));
        assertEquals(listItem3, dLinkedList.getTail());
        assertEquals(1, dLinkedList.getCount());
        assertFalse(dLinkedList.removeListItem(listItem2));
    }

    @Test
    public void testAddItemGetTail() {
        assertNull(dLinkedList.getTail());
        dLinkedList.addItem(listItem1);
        assertEquals(listItem1, dLinkedList.getTail());
    }

    @Test
    public void testAddItemRemoveListItemGetCount() {
        assertEquals(0, dLinkedList.getCount());
        assertNotNull(dLinkedList.addItem(listItem1));
        assertEquals(1, dLinkedList.getCount());
        assertTrue(dLinkedList.removeListItem(listItem1));
        assertEquals(0, dLinkedList.getCount());
    }

    @Test
    public void testAddItemsRefreshListItem() {
        List<IListItem<Integer, Person>> items = dLinkedList.addItems(listItem1, listItem2, listItem3);
        assertEquals(3, items.size());
        assertEquals(listItem1, dLinkedList.getHead());
        assertEquals(listItem3, dLinkedList.getTail());
        dLinkedList.refreshListItem(listItem1);
        assertEquals(listItem2, dLinkedList.getHead());
        assertEquals(listItem1, dLinkedList.getTail());
    }
}