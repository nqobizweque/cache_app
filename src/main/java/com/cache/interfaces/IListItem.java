package com.cache.interfaces;

import com.cache.app.ListItem;

public interface IListItem<K, T> {
    T getItem();

    IListItem<K, T> getHead();

    void setHead(IListItem<K, T> head);

    IListItem<K, T> getTail();

    void setTail(IListItem<K, T> tail);

    K getKey();

    void setKey(K key);

    void clear();
}
