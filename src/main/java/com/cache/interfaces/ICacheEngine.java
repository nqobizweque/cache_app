package com.cache.interfaces;

public interface ICacheEngine<K, T> {
    boolean containsItem(K key);

    T getItem(K key);

    boolean cacheItem(K key, T item);

    boolean pruneItem();
}
