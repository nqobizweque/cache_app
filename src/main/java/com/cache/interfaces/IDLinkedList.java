package com.cache.interfaces;

import com.cache.app.ListItem;

import java.util.List;

public interface IDLinkedList<K, T> {

    IListItem<K, T> getHead();

    IListItem<K, T> getTail();

    int getCount();

    IListItem<K, T> addItem(IListItem<K, T> listItem);

    List<IListItem<K, T>> addItems(IListItem<K, T>... listItems);

    boolean removeListItem(IListItem<K, T> listItem);

    boolean refreshListItem(IListItem<K, T> listItem);
}
