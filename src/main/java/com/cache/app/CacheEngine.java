package com.cache.app;

import com.cache.interfaces.ICacheEngine;
import com.cache.interfaces.IListItem;

import java.util.Hashtable;

public class CacheEngine<K, T> implements ICacheEngine<K, T> {

    public DLinkedList<K, T> linkedList;
    public Hashtable<K, IListItem<K, T>> index;

    public CacheEngine() {
        linkedList = new DLinkedList();
        index = new Hashtable<K, IListItem<K, T>>();
    }

    public boolean containsItem(K key) {
        return index.containsKey(key);
    }

    public T getItem(K key) {
        if (!index.containsKey(key)) {
            return null;
        }
        IListItem<K, T> listItem = index.get(key);
        linkedList.refreshListItem(listItem);
        return listItem.getItem();
    }

    public boolean cacheItem(K key, T item) {
        if (index.containsKey(key)) {
            return false;
        }
        IListItem<K, T> listItem = linkedList.addItem(new ListItem<K, T>(key, item));
        index.put(key, listItem);
        return true;
    }

    public boolean pruneItem() {
        IListItem<K, T> head = linkedList.getHead();
        if (head == null) {
            return false;
        }
        K key = head.getKey();
        linkedList.removeListItem(head);
        return head == index.remove(key);
    }
}
