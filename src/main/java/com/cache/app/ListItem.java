package com.cache.app;

import com.cache.interfaces.IListItem;

public class ListItem<K, T> implements IListItem<K, T> {

    private final T item;
    private K key;
    private IListItem<K, T> head;
    private IListItem<K, T> tail;

    public ListItem(K key, T item) {
        this.key = key;
        this.item = item;
    }

    public T getItem() {
        return item;
    }

    public IListItem<K, T> getHead() {
        return head;
    }

    public void setHead(IListItem<K, T> head) {
        this.head = head;
    }

    public IListItem<K, T> getTail() {
        return tail;
    }

    public void setTail(IListItem<K, T> tail) {
        this.tail = tail;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public void clear() {
        head = null;
        tail = null;
        key = null;
    }
}
