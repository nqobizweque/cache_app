package com.cache.app;

public class Person {
    private final int ID;
    private final String Name;

    public Person(int id, String name) {
        ID = id;
        Name = name;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return Name;
    }
}
