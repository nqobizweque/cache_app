package com.cache.app;

import com.cache.interfaces.IDLinkedList;
import com.cache.interfaces.IListItem;

import java.util.Arrays;
import java.util.List;

public class DLinkedList<K, T> implements IDLinkedList<K, T> {
    private IListItem<K, T> head;
    private IListItem<K, T> tail;
    private int count;

    public DLinkedList() {
        count = 0;
    }

    public IListItem<K, T> getHead() {
        return head;
    }

    public IListItem<K, T> getTail() {
        return tail;
    }

    public int getCount() {
        return count;
    }

    public IListItem<K, T> addItem(IListItem<K, T> listItem) {
        count += 1;
        if (tail == null) {
            tail = head = listItem;
            return listItem;
        }
        listItem.setHead(tail);
        tail.setTail(listItem);
        return tail = listItem;
    }

    public List<IListItem<K, T>> addItems(IListItem<K, T>... listItems) {
        if (listItems == null) {
            return null;
        }
        for (IListItem<K, T> listItem: listItems) {
            addItem(listItem);
        }
        return Arrays.asList(listItems);
    }

    public boolean removeListItem(IListItem<K, T> listItem) {
        if (listItem == null || tail == null) {
            return false;
        }

        if ((listItem != head && listItem.getHead() == null) || (listItem != tail && listItem.getTail() == null)) {
            return false;
        }

        count -= 1;
        if (listItem == head && listItem == tail) {
            listItem.clear();
            tail = head = null;
            return true;
        }
        if (listItem == head) {
            IListItem<K, T> curHeadTail = head.getTail();
            curHeadTail.setHead(null);
            head.clear();
            head = curHeadTail;
            return true;
        }
        if (listItem == tail) {
            IListItem<K, T> curHeadTail = head.getTail();
            curHeadTail.setHead(null);
            head.clear();
            head = curHeadTail;
            return true;
        }
        IListItem<K, T> curHead = listItem.getHead(), curTail = listItem.getTail();
        curHead.setTail(curTail);
        curTail.setHead(curHead);
        listItem.clear();
        return true;
    }

    public boolean refreshListItem(IListItem<K, T> listItem) {
        if (listItem == null || tail == null || (listItem.getTail() == null && listItem.getHead() == null)) {
            return false;
        }
        count -= 1;
        if ((listItem == head && listItem == tail) || listItem == tail) {
            return true;
        }
        removeListItem(listItem);
        listItem.setHead(tail);
        tail.setTail(listItem);
        tail = listItem;
        return true;
    }
}